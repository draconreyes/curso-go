package mypackage

import "fmt"

//CarPublic Car con acceso publico
type CarPublic struct {
	Brand string
	Year int
}
//carPrivate Car con acceso privado
type carPrivate struct {
	brand string
	year int
}
//PrintMessage  imprimir un mensaje
func PrintMessage(text string){
	fmt.Println(text)
}