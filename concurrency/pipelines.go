package main

import "fmt"

func Generator(c chan<- int) {
	for i := 0; i < 10; i++ {
		c <- i
	}
	close(c)
}

func Double(cIn <-chan int, cOut chan<- int) {
	for v := range cIn {
		cOut <- 2 * v
	}

	close(cOut)
}

func Print(c <-chan int) {
	for value := range c {
		fmt.Printf("#%d\n", value)
	}
}

func main() {
	generator := make(chan int)
	doubles := make(chan int)

	go Generator(generator)
	go Double(generator, doubles)
	Print(doubles)
}
