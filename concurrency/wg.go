package main

import (
	"fmt"
	"sync"
	"time"
)

/*
creates a wait group that gets incremented by on on every iteration, which
will as well run the doSmth() func that will substract 1 from the wait group
after it finishes. wg.Wait() at the end guarantees that it'll wait for the wg (counter)
to be 0.
*/

func doSmth(u int, wg *sync.WaitGroup) {
	defer wg.Done()

	fmt.Printf("Started at #%d\n", u)
	time.Sleep(time.Second * 2)
	fmt.Println("Ended...")
}

func main() {

	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go doSmth(i, &wg)
	}

	wg.Wait()
}
